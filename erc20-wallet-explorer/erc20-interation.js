// change this to your contract address
var tokenAddress = "0x2D05e613003951020F42a54978493Cdd02351C7a";

// web3 instance
var web3 = new Web3(new Web3.providers.HttpProvider("https://ropsten.infura.io/"));

$('#tokenForm').submit(function (event) {

	// preventDefault on the submit event to prevent form from carrying out its default action
	
	event.preventDefault();

	// set our results div to show a loading status

	$('#results').html('loading...');

	// create a web3 contract object with the ERC20 abi

	// var tokenAddress = $('#contractAddress').val();
	var token = web3.eth.contract(erc20Abi).at(tokenAddress);

	// fetch data from the blockchain

	// 1. get the total supply
	token.totalSupply.call(function (err, totalSupply) {
			
		// 2. get the number of decimal places used to represent this token
		token.decimals.call(function (err, decimals) {

			// 3. get the name of the token
			token.name.call(function (err, name) {

				// 3. get the balance of the account holder
				var accountAddress = $('#accountAddress').val();

				token.balanceOf.call(accountAddress, function (err, balance) {

					// update the UI to reflect the data returned from the blockchain
					var percentOwned = balance.div(totalSupply).mul(100);

					var divisor = new web3.BigNumber(10).toPower(decimals);
					totalSupply = totalSupply.div(divisor);
					balance = balance.div(divisor);

					var results = '<b>Token:</b> ' + name + '<br />';
					results += '<b>Total supply:</b> ' + totalSupply.round(5) + '<br /><br />';
					results += '<b>' + accountAddress + '</b>'
					 + ' owns ' + '<b>' + balance.round(5) + '</b>'
					 + ' TOPs which is ' + '<b>' + percentOwned.round(5) + '</b>'
					 + '% of the total supply.';

					$('#results').html(results);
				});

			});

		});

	});
});